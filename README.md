# Basic Exercise

For this exercise, you will produce a Spring Boot application written in
Java, Groovy or Kotlin that implements a REST service backed by a database. 
If you are unfamiliar with Spring Boot, you may find the 
[Spring Initializr](https://start.spring.io/) site to be a good kickstart 
for your project.

The NIH RxNorm data contains data about prescription medications. Of 
particular interest for this exercise is that it provides a means to map 
generic to brand names for a medication.

The deliverable for this exercise is a REST service that takes a RxNorm 
concept unique identifier (RXCUI) and returns the RXCUI codes for the 
generic version as well as all brand name versions. The code parameter 
provided to the service can be that of a generic version or the brand 
name version. The service should return the code provided to the service 
as well as all others that meet the criteria. For example, if you pass a 
RXCUI code for Cymbalta (482574), the service should return the codes 
for duloxetine hydrochloride (72625), and for Irenka (1652064), and of 
course Cymbalta (482574).

To get started, download the [RxNorm data from the NIH site](https://www.nlm.nih.gov/research/umls/rxnorm/docs/rxnormfiles.html). 
The RxNorm download page contains a complete data set but for this exercise we are 
only interested in a subset of that data which does not require a license 
to download. Look for a section of the page entitled "Current Prescribable 
Content Monthly Release". Download the first link in this section 
(something with a name like `RxNorm_full_prescribe_09062016.zip` should be listed). 

The zip file contains scripts to load the data into either a MySQL or 
Oracle instance. You may use either database or another data store entirely 
(for example, a NoSQL database).

Two documents will be of helpful for completing this exercise:

- [RxNorm Technical Documentation](https://www.nlm.nih.gov/research/umls/rxnorm/docs/2016/rxnorm_doco_full_2016-2.html)
(Note: this describes the full data set but also applies to the data provided in 
the subset we are using)
- [RxNorm Term Types (TTY)](https://www.nlm.nih.gov/research/umls/rxnorm/docs/2016/appendix5.html)

Please do not spend more than 4-8 hours to complete this exercise. Our 
goal is to get a sense of how you approach a problem and the choices you 
make along the way rather than to deliver a complete solution. We prefer 
a well considered solution that better reflects your work than something 
that works but is rushed and poorly executed. That said, we’d like to see 
what you can do with an unfamiliar data set in a limited timeframe.

If you have any questions about the data or instructions, please contact 
us. We’re happy to help clarify and provide pointers.

# Stretch Exercise

This exercise is purely optional and only meant to be done in the case
that you've completed the basic exercise and want to show more of your 
work. Not completing this exercise will *not* reflect poorly on your 
submission.

The objective in this exercise is to determine the available strengths
of a given medication. So for a specific RXCUI, we would like a list of 
all of the dosage strengths of which the medication is available. For 
example, if provided the RXCUI value for Cymbalta (482574), the service 
should return the following list:

- 20 MG
- 30 MG
- 40 MG
- 60 MG

As with the first exercise, please let us know if the instructions need
clarification.